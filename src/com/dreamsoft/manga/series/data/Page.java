/*
 * Page.java
 *
 * Copyright by Orell F�ssli Wirtschaftsinformationen AG
 * Z�rich
 * All rights reserved.
 */
package com.dreamsoft.manga.series.data;

/**
 * 
 *
 * @author lptchi
 * @version $Revision:  $
 */
public class Page
{
    private String id;
    private String urlImage;
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }
    
    /**
     * Sets the id.
     *
     * @param id the id
     * @return the page
     */
    public Page setId(String id)
    {
        this.id = id;
        return this;
    }
    
    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrlImage()
    {
        return urlImage;
    }
    
    /**
     * Sets the url.
     *
     * @param url the url
     * @return the page
     */
    public Page setUrlImage(String url)
    {
        this.urlImage = url;
        return this;
    }
}


/*
 * Changes:
 * $Log: $
 */