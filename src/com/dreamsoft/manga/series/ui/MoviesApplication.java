package com.dreamsoft.manga.series.ui;

import java.util.List;

import android.app.Application;

import com.dreamsoft.manga.series.data.Movie;

public class MoviesApplication extends Application {

	private List<Movie> mDemoData;

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public List<Movie> getDemoData() {
		return mDemoData;
	}
}
