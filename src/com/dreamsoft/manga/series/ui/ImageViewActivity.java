
package com.dreamsoft.manga.series.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dreamsoft.manga.series.data.Page;
import com.dreamsoft.manga.series.util.HttpUtil;
import com.dreamsoft.manga.series.R;
import com.fasterxml.jackson.databind.JsonNode;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;
public class ImageViewActivity extends Activity{
	private static final String STATE_POSITION = "STATE_POSITION";
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	ViewPager pager;
	DisplayImageOptions options;
	class PageOnlineTask extends AsyncTask<String, Void, List<Page>>{
    	@Override
    	protected List<Page> doInBackground(String... params) {
    		List<Page> res = new ArrayList<Page>();
    		JsonNode jnode = HttpUtil.getJson(params[0]);
    		if (jnode != null){
    			Iterator<JsonNode> it = jnode.iterator();
    			int id = 0;
    			while(it.hasNext()){
    				JsonNode n = it.next();
    				if (n.get("img") != null){
    					Page page = new Page();
    					page.setUrlImage(n.get("img").asText());
    					page.setId(String.valueOf(id));
    					id ++;
    					res.add(page);
    				}
    			}
    		}
    		return res;
    	}

    	@Override
    	protected void onPostExecute(List<Page> result) {
    		List<String> urlImages = new ArrayList<String>();
			for (Page page : result)
			{
				urlImages.add(page.getUrlImage());
			}
			if (urlImages.size() == 0){
				Toast.makeText(ImageViewActivity.this, "error loading chap", Toast.LENGTH_LONG).show();
			}else{
				displayImages(urlImages.toArray(new String[urlImages.size()]), false);
			}
			
    	}

    	@Override
    	protected void onPreExecute() {
    	}

    	@Override
    	protected void onProgressUpdate(Void... values) {
    	}
	}
	/*private void loadAdview(View v) {
		BannerView mBanner = (BannerView) v.findViewById(R.id.bannerViewpageAC);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(MoviesActivity.adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}*/
	private void displayPager(Bundle savedInstanceState)
	{
		setContentView(R.layout.ac_image_pager);
		/*requestWindowFeature(Window.FEATURE_NO_TITLE);
	     getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
		Intent intent = getIntent();
		String jsonText = String.valueOf(intent.getStringExtra("url"));
        new PageOnlineTask().execute(jsonText);
	}
	private void displayImages(String[] images, boolean isDownload)
	{
		int pagerPosition = 0;
		
		options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.loading)
			.showImageForEmptyUri(R.drawable.loading)
			.showImageOnFail(R.drawable.loading)
			.resetViewBeforeLoading(true)
			.cacheOnDisc(true)
			.imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.considerExifParams(true)
			.displayer(new FadeInBitmapDisplayer(300))
			.build();
		pager = (ViewPager) findViewById(R.id.pagerAC);
		pager.setAdapter(new ImagePagerAdapter(images, isDownload));
		pager.setCurrentItem(pagerPosition);
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		try{
		outState.putInt(STATE_POSITION, pager.getCurrentItem());
		}catch(Exception e){
			Toast.makeText(ImageViewActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
		}
	}
	private class ImagePagerAdapter extends PagerAdapter {

		private String[] images;
		private LayoutInflater inflater;
		private boolean isDownload;
		ImagePagerAdapter(String[] images, boolean isDownload) {
			this.images = images;
			this.isDownload = isDownload;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return images.length;
		}

		@Override
		public Object instantiateItem(ViewGroup view, int position) {
			View imageLayout = inflater.inflate(R.layout.viewimage, view, false);
			assert imageLayout != null;
			ImageView imageView = (ImageView) imageLayout.findViewById(R.id.iv_photo);
			final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading2);
			ImageLoaderConfiguration ld = ImageLoaderConfiguration.createDefault(getApplicationContext());
			imageLoader.init(ld);
			imageLoader.displayImage(images[position], imageView, options, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					Toast.makeText(ImageViewActivity.this, message, Toast.LENGTH_SHORT).show();

					spinner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
			});
			//loadAdview(imageLayout);
			view.addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		displayPager(savedInstanceState);
	}
}