package com.dreamsoft.manga.series.ui;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import uk.co.senab.photoview.PhotoView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;

import com.dreamsoft.manga.series.data.Page;
import com.dreamsoft.manga.series.R;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

public class ViewPagerActivity extends Activity {
	protected static final String TAG = null;
	private ProgressDialog progress;
	private void showPleaseWait()
	{
	}
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewpageAC);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(MoviesActivity.adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	private void hidePleaseWait()
	{
		if (progress != null)
		{
			progress.dismiss();
			progress = null;
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	     getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_pager);        
        showPleaseWait();
        Intent intent = getIntent();
        String downloaded = intent.getStringExtra("downloaded");
        intent.getStringExtra("admobId");
        if (downloaded == null)
        {
	        String jsonText = "{\"url\":\"" + intent.getStringExtra("url") + "\"}";
	        new PageOnlineTask().execute("contents", jsonText);
        }
        else
        {
            SamplePagerAdapter samplePagerAdapter = new SamplePagerAdapter(null);
        	File folder = new File(downloaded);
        	File[] listOfFiles = folder.listFiles();
        	if (listOfFiles.length > 1)
        	{
	        	File[] files = new File[listOfFiles.length - 1];
	        	for (int i = 1; i < listOfFiles.length; i++)
	        	{
	        		files[i-1] = listOfFiles[i];
	        	}
	        	samplePagerAdapter.setListDownload(files);
        	}
        	ViewPager mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
    		//setContentView(mViewPager);
    		mViewPager.setAdapter(samplePagerAdapter);
    		loadAdview();
    		hidePleaseWait();
        }
       
	}
	@Override
    public void onDestroy()
    {
        getIntent().removeExtra("downloaded");
        getIntent().removeExtra("url");
        super.onDestroy();
    }
	static class SamplePagerAdapter extends PagerAdapter {
		private static String[] arr;
		private static File[] downloadedBitmaps;
		public void setListDownload(File[] listOfFile)
		{
			downloadedBitmaps = listOfFile;
		}
		public void setListUrl(String[] bmps)
		{
			arr = bmps;
		}
		public SamplePagerAdapter(String[] lst)
		{
			arr = lst;
		}
		
		@Override
		public int getCount() {
			if (arr != null)
				return arr.length;
			if (downloadedBitmaps != null)
				return downloadedBitmaps.length;
			return 0;
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			PhotoView photoView = new PhotoView(container.getContext());
			if (downloadedBitmaps != null)
			{
				Drawable drw = Drawable.createFromPath(downloadedBitmaps[position].getAbsolutePath());
				photoView.setImageDrawable(drw);
			}
			else
			{
				UrlImageViewHelper.setUrlDrawable(photoView, arr[position],R.drawable.loading);
			}
			// Now just add PhotoView to ViewPager and return it
			container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

	}
	class PageOnlineTask extends AsyncTask<String, Void, List<Page>>{
    	@Override
    	protected List<Page> doInBackground(String... params) {
    		List<Page> res = new ArrayList<Page>();
    		return res;
    	}

    	@Override
    	protected void onPostExecute(List<Page> result) {
            SamplePagerAdapter samplePagerAdapter = new SamplePagerAdapter(null);
    		List<String> urlImages = new ArrayList<String>();
			for (Page page : result)
			{
				urlImages.add(page.getUrlImage());
			}
			samplePagerAdapter.setListUrl(urlImages.toArray(new String[urlImages.size()]));
			samplePagerAdapter.setListDownload(null);
			ViewPager mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
    		//setContentView(mViewPager);
    		mViewPager.setAdapter(samplePagerAdapter);
    		loadAdview();
    		hidePleaseWait();
    	}

    	@Override
    	protected void onPreExecute() {
    	}

    	@Override
    	protected void onProgressUpdate(Void... values) {
    	}
	}
}
