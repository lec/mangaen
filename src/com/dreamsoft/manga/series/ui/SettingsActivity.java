package com.dreamsoft.manga.series.ui;

import java.io.File;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

import com.dreamsoft.manga.series.data.Setting;
import com.dreamsoft.manga.series.util.MockUtils;
import com.dreamsoft.manga.series.R;

public class SettingsActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		Button btn = (Button)findViewById(R.id.resetBtn);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				File dir = new File(MoviesActivity.rootPath + getString(R.string.movieFolder));
				if (dir != null){
					if (dir.isDirectory() && dir.listFiles() == null){
						dir.delete();
					}else{
						for (File f : dir.listFiles()){
							f.delete();
						}
						dir.delete();
					}
				}
				System.exit(0);
			}
		});
		
		CheckBox chk = (CheckBox)findViewById(R.id.shuffleFilm);
		if (MoviesActivity.setting != null){
			chk.setChecked(MoviesActivity.setting.isShuffle());
		}
		else{
			MoviesActivity.setting = new Setting();
			chk.setChecked(false);
		}
		chk.setOnClickListener(new OnClickListener() {
			  @Override
			  public void onClick(View v) {
		                //is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					MoviesActivity.setting.setShuffle(true);
				}
				else{
					MoviesActivity.setting.setShuffle(false);
				}
				try{
					MockUtils.writeDataToFileSystem(MoviesActivity.settingPath, MoviesActivity.setting);
				}catch(Exception e){
					e.printStackTrace();
				}
					
			  }
		});
	}

}
