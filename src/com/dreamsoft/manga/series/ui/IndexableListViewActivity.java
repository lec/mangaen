package com.dreamsoft.manga.series.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamsoft.manga.series.data.Movie;
import com.dreamsoft.manga.series.util.HttpUtil;
import com.dreamsoft.manga.series.R;
import com.fasterxml.jackson.databind.JsonNode;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;


public class IndexableListViewActivity extends Activity {
	private ArrayList<String> mItems;
	private ArrayList<Movie> mMovies;
	private ListView mListView;
	private ProgressDialog progress;
	private String poster = "";
	private String title = "";
	private String des = "";
	private String linkManga = "http://phimhdplus.com/mangaen/api/chapter/";
	private String linkContent = "http://phimhdplus.com/mangaen/api/content/";
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlistindex);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(MoviesActivity.adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	private void showPleaseWait()
	{
		progress = ProgressDialog.show(this, getString(R.string.loadingTilte),
        	    getString(R.string.loadingWelcome), true);
	}
	private void hidePleaseWait()
	{
		if (progress != null)
    	{
    		progress.dismiss();
    		progress = null;
    	}
	}
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.indexable_list_movies);
        showPleaseWait();
        loadAdview();
		Intent intent = getIntent();
		int id = intent.getIntExtra("id", -1);
		poster = intent.getStringExtra("img");
		title = intent.getStringExtra("name");
		if(id > 0)
		{
			GetMovie gM = new GetMovie();
			gM.execute(linkManga + id);
		}else
		{
			Toast.makeText(this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			hidePleaseWait();
		}
    }
    
    class GetMovie extends AsyncTask<String, Void, JsonNode>{
		@Override
		protected JsonNode doInBackground(String... params) {
    		return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			if(result != null)
			{
				loadFilmBo(result);
			}else
			{
				Toast.makeText(IndexableListViewActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
				hidePleaseWait();
			}
		}
	}
    
    private void loadFilmBo(JsonNode node)
    {
    	mItems = new ArrayList<String>();
    	mMovies = new ArrayList<Movie>();
    	if (node.get("details") != null){
    		des = node.get("details").asText();
    	}
    	if (node.get("list") != null){
    		JsonNode rs = node.get("list");
    		Iterator<JsonNode> it = rs.iterator();
    		while(it.hasNext())
    		{
    			JsonNode n = it.next();
    			Movie mo = new Movie();
    			if (n.get("name") != null){
    				mo.setTitle(n.get("name").asText());
    				mItems.add(n.get("name").asText());
    			}
    			if (n.get("id") != null){
    				mo.setChapId(n.get("id").asInt());
    			}
    			mMovies.add(mo);
    		}
    	}    	
    	getIntent();
    	ImageView imageView = (ImageView) findViewById(R.id.poster_view2);
    	UrlImageViewHelper.setUrlDrawable(imageView, poster, R.drawable.logohdplus);
    	final TextView titletv = (TextView) findViewById(R.id.title_view2);
    	titletv.setText(title);
    	TextView desc = (TextView) findViewById(R.id.subtitle_view2);
    	desc.setText(des);
    	desc.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	TextView tv = (TextView)v;
            	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
        				IndexableListViewActivity.this);
        			// set title
        			alertDialogBuilder.setTitle(titletv.getText());
        			alertDialogBuilder.setPositiveButton("OK", null);
        			// set dialog message
        			alertDialogBuilder
        				.setMessage(tv.getText());
        				AlertDialog alertDialog = alertDialogBuilder.create();
        			alertDialog.show();
            }
        });
        Collections.sort(mItems);
        ArrayAdapter<String> adt = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mItems.toArray(new  String[mItems.size()]));
       
        mListView = (ListView) findViewById(R.id.listview);
        mListView.setAdapter(adt);
        mListView.setFastScrollEnabled(true);
        mListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				showPleaseWait();
				TextView tv = (TextView) arg1;
				String chap = tv.getText().toString().trim().toUpperCase();
				Movie curr = null;
				for (Movie m : mMovies){
					if (chap.equalsIgnoreCase(m.getTitle().trim().toUpperCase())){
						curr = m;
						break;
					}
				}
				if (curr != null){
					hidePleaseWait();
					Intent itent = new Intent(IndexableListViewActivity.this, ImageViewActivity.class);
					itent.putExtra("url", linkContent + curr.getChapId());
					startActivity(itent);
				}
			}
		});
		hidePleaseWait();
    }
}