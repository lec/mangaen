package com.dreamsoft.manga.series.ui;

import com.dreamsoft.manga.series.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextThemeWrapper;

public class AboutDialogFragment extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		ContextThemeWrapper context = new ContextThemeWrapper(getActivity(),
				android.R.style.Theme_Holo_Light_Dialog);
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setMessage(R.string.about_msg);
		builder.setNeutralButton(android.R.string.ok, null);
		
		return builder.create();
	}
	
	
}
