package com.dreamsoft.manga.series.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.darvds.ribbonmenu.RibbonMenuView;
import com.darvds.ribbonmenu.iRibbonMenuCallback;
import com.dreamsoft.manga.series.R;
import com.dreamsoft.manga.series.data.Movie;
import com.dreamsoft.manga.series.data.Setting;
import com.dreamsoft.manga.series.data.UpdateApp;
import com.dreamsoft.manga.series.util.Converter;
import com.dreamsoft.manga.series.util.HttpUtil;
import com.dreamsoft.manga.series.util.InternetUtil;
import com.dreamsoft.manga.series.util.MockUtils;
import com.dreamsoft.manga.series.util.Vietnamese;
import com.dreamsoft.manga.series.view.SlidingListView;
import com.dreamsoft.manga.series.view.ViewPager;
import com.dreamsoft.manga.series.widget.MovieAdapter;
import com.dreamsoft.manga.series.widget.MovieAdapter.MovieAdapterListener;
import com.fasterxml.jackson.databind.JsonNode;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;
@TargetApi(11)
public class MoviesActivity extends FragmentActivity implements ActionBar.TabListener,
		MovieAdapter.MovieAdapterListener, iRibbonMenuCallback {
	private RibbonMenuView rbmView;
	private ProgressDialog progress;
	private InternetUtil internet;
	SectionsPagerAdapter mSectionsPagerAdapter;
	public static Setting setting = null;
	public static String settingPath = "";
	public  String PUBLIC_KEY = "http://phimhdplus.com/api/cc4fa29ed148b0c5d826777d65eb76f8";

	private String truyenTranhLink = "http://phimhdplus.com/mangaen/api/1";
	private String truyenMoi = "http://phimhdplus.com/mangaen/api/2";
	private String dsMoiRa = "http://phimhdplus.com/mangaen/api/3";
	ViewPager mViewPager;
	public static String[] COUNTRIES = new String[] {};
	// ListView on screen
	private SlidingListView mListView;
	private MovieAdapter mAdapter;
	public static String rootPath;
	public static LayoutInflater inflator;
	public static List<Movie> search = new ArrayList<Movie>();
	public static List<Movie> truyen = new ArrayList<Movie>();
	public static List<Movie> newest = new ArrayList<Movie>();
	
	public static List<Movie> later = new ArrayList<Movie>();
	public static int adId = 65832139;

	public void showUpdateApp(String type, String message, final String url)
	{
		if (type == null || (type != null && type.trim().equals("")))
		{
			return;
		}
		AlertDialog alertDialog = new AlertDialog.Builder(MoviesActivity.this).create();
		alertDialog.setIcon(R.drawable.setting);
		alertDialog.setTitle("Notice");
		alertDialog.setMessage(message);
		if (type.trim().equals("YES|NO"))
		{
			alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
				}
			});
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					openPlayStore(url);
				}
			});
		}
		if (type.trim().equals("YES"))
		{
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					openPlayStore(url);
				}
			});
		}
		alertDialog.show();
	}
	private void openPlayStore(String url)
	{
		if (url == null || (url != null && url.isEmpty())){
			return;
		}
		if(url.startsWith("market://details")){
			String appPackageName = url;
			try {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appPackageName)));
			} catch (android.content.ActivityNotFoundException anfe) {
				appPackageName = appPackageName.replace("market://details?id=", "");
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
			}
		}else{
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
		}
	}
	class UpdateAppTask extends AsyncTask<String, Void, UpdateApp>{
		@Override
		protected UpdateApp doInBackground(String... params) {
			if (internet.haveNetworkConnection())
			{
				return Converter.json2UpdateApp(HttpUtil.getJson(params[0]));
			}
			return null;
		}
	}
	public void doUpdateApp(){
		if (internet.haveNetworkConnection()){
			UpdateAppTask utk = new UpdateAppTask();
			try {
				UpdateApp updateApp = utk.execute(PUBLIC_KEY).get();
				if (updateApp != null){
					if ("1".equals(updateApp.getType())){
						showUpdateApp("YES|NO", updateApp.getMessage(), updateApp.getAction());
					}
					if ("2".equals(updateApp.getType())){
						showUpdateApp("YES", updateApp.getMessage(), updateApp.getAction());
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(adId);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	private void loadRightMenu(){
		rbmView = (RibbonMenuView) findViewById(R.id.ribbonMenuView1);
        rbmView.setMenuClickCallback(this);
        rbmView.setMenuItems(R.menu.ribbon_menu);
        
         getActionBar().setDisplayHomeAsUpEnabled(true);
         getActionBar().setHomeButtonEnabled(true);

	}
	
	public void adapAutocomplete(){
        getActionBar().setDisplayShowCustomEnabled(true);
		List<String> l = new ArrayList<String>();
		for (Movie m : search){
			l.add(m.getTitle());
			String title = m.getTitle();
			String normalTitle = Vietnamese.toUrlFriendly(m.getTitle());
			boolean dup = false;
			if (title != null && normalTitle != null && title.trim().toLowerCase().equals(normalTitle.trim().toLowerCase())){
				dup = true;
			}
			if (!dup){
				l.add(Vietnamese.toUrlFriendly(m.getTitle()));
			}
		}
		COUNTRIES = l.toArray(new String[l.size()]);
		View v = inflator.inflate(R.layout.autocomplete, null);
        getActionBar().setCustomView(v);
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MoviesActivity.this,
                android.R.layout.simple_dropdown_item_1line, COUNTRIES);
        
        AutoCompleteTextView textView = (AutoCompleteTextView) v
                .findViewById(R.id.editText1);
        textView.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(textView, InputMethodManager.SHOW_IMPLICIT);
        textView.setOnItemClickListener(searchAutocomplete);
        textView.setAdapter(adapter);
	}
	public AdapterView.OnItemClickListener searchAutocomplete =  new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			TextView tv = (TextView)arg1;
			String search = tv.getText().toString().trim().toLowerCase();
			int id = 0;
			for(Movie m : MoviesActivity.search){
				String title = m.getTitle();
				if (title.trim().toLowerCase().equals(search) 
						|| (Vietnamese.toUrlFriendly(title)).trim().toLowerCase().equals(search)){
					id = m.getId();
					break;
				}
			}
			if (id > 0){
				/*showPleaseWait();
				GetMovie gM = new GetMovie();
				gM.execute("http://phimhdplus.com/movies/api/details/" + id);*/
				if (internet.haveNetworkConnection()){
					Intent itent = new Intent(MoviesActivity.this, IndexableListViewActivity.class);
					itent.putExtra("id", id);
					startActivity(itent);
				}
				else
				{
					Toast.makeText(MoviesActivity.this, getString(R.string.noInternet), Toast.LENGTH_LONG).show();
				}
			}
		}
    	
	};
	private String getRootPathStorage()
	{
		String path = null;
		Boolean isSDPresent = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	    if(isSDPresent)
	    {
	      path = Environment.getExternalStorageDirectory().getAbsolutePath();
	    }
	    else
	    {
	        path = getFilesDir().getAbsolutePath();
	    }
	    return path;
	}
	private void loadMovies() {
		File dir = new File(rootPath + getString(R.string.movieFolder));
		File data = new File(rootPath + getString(R.string.movieData));
		if (!dir.exists()|| !data.exists())
		{
			//first time load 
			if (internet.haveNetworkConnection())
			{
				dir.mkdir();
				GetMovies gMovie = new GetMovies();
				gMovie.setLoadImediatly(true);
				gMovie.execute(truyenTranhLink);
			}
			else
			{
				Toast.makeText(this, getString(R.string.noInternet), Toast.LENGTH_SHORT).show();
				hidePleaseWait();
				return;
			}
		}
		else
		{
			GetMoviesOffline getOffline = new GetMoviesOffline();
			getOffline.execute("getoffline");
		}
	}
	/*private void loadMovies(String filmType, String source, String file) {
		File dir = new File(rootPath + getString(R.string.movieFolder));
		File data = new File(rootPath + file);
		if (!dir.exists() || !data.exists())
		{
			//first time load 
			if (internet.haveNetworkConnection())
			{
				dir.mkdir();
				GetMoviesOther gMovie = new GetMoviesOther();
				gMovie.setLoadImediatly(true);
				gMovie.execute(filmType, file,source);
			}
			else
			{
				Toast.makeText(this, getString(R.string.noInternet), Toast.LENGTH_SHORT).show();
				hidePleaseWait();
				return;
			}
		}
		else
		{
			GetMoviesOfflineOther getOffline = new GetMoviesOfflineOther();
			getOffline.execute(file, source);
		}
	}*/
    private void showPleaseWait()
	{
    	if ( progress!=null && progress.isShowing() ){
    		progress.cancel();
        }
		progress = ProgressDialog.show(MoviesActivity.this, getString(R.string.loadingTilte),
        	    getString(R.string.loadingWelcome), true);
	}
	private void hidePleaseWait()
	{
		if (progress != null)
    	{
    		progress.dismiss();
    		progress = null;
    	}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movies);
		showPleaseWait();
		loadAdview();
		loadRightMenu();
		internet = new InternetUtil();
		internet.setActivity(this);
		rootPath = getRootPathStorage();
		settingPath = rootPath + getString(R.string.settingData);
		
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOffscreenPageLimit(3);

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(
					actionBar.newTab()
							.setText(mSectionsPagerAdapter.getPageTitle(i))
							.setTabListener(this));
		}
		mViewPager.setCurrentItem(1);
		actionBar.setSelectedNavigationItem(1);
        inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        
		mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			private int mLastState;

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				if (position == 0) {
					setSlide(positionOffset - 1);
				}
				else if (position == 1) {
					setSlide(positionOffset);
				}
				else if (position == 2) {
					setSlide(1);
				}
			}

			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					mListView.setUseHardwareLayers(false);
				}
				else if (mLastState == ViewPager.SCROLL_STATE_IDLE) {
					mListView.setUseHardwareLayers(true);
				}

				mLastState = state;
			}
		});

		mListView = (SlidingListView) findViewById(R.id.sliding_list_view);

		// Add some spacing views above/below the rest of the rows; this keeps us from having
		// to customize the first/last rows to add some extra padding
		LayoutInflater inflater = LayoutInflater.from(this);
		mListView.addHeaderView(inflater.inflate(R.layout.include_header_footer_space, mListView, false));
		mListView.addFooterView(inflater.inflate(R.layout.include_header_footer_space, mListView, false));

		// We want the total width == 3*cell padding + 2*cell size
		int cellPadding = getResources().getDimensionPixelSize(R.dimen.cell_padding);
		Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		cellSize = (size.x - 3 * cellPadding) / 2;
		movieAdapterListener = this;
		mAdapter = new MovieAdapter(MoviesActivity.this, new ArrayList<Movie>(), this, cellSize);
		loadMovies();
	}
	private MovieAdapterListener movieAdapterListener;
	private int cellSize;
	private void setSlide(float slide) {
		mAdapter.setSlide(slide);
		mListView.setSlide(slide);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.movies, menu);
		return true;
	}
	int check = 0;
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_rate:
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=" + getApplication().getPackageName()));
				startActivity(intent);
				return true;
			case R.id.action_about:
				DialogFragment df = new AboutDialogFragment();
				df.show(getSupportFragmentManager(), "aboutDf");
				
				return true;
			case R.id.action_web:
				Intent intentweb = new Intent(Intent.ACTION_VIEW);
				intentweb.setData(Uri.parse("http://phimhdplus.com"));
				startActivity(intentweb);
				return true;
			case R.id.action_search:
				if (check == 0)
				{
					adapAutocomplete();
					check = 1;
				}
				else{
					
					check = 0;
					getActionBar().setDisplayShowCustomEnabled(false);
				}
				return true;
			case android.R.id.home:
				rbmView.toggleMenu();
				return true;				
			case R.id.ribbon_menu_settings:
				Intent ta = new Intent(MoviesActivity.this, SettingsActivity.class);
				startActivity(ta);
				break;
			case R.id.ribbon_menu_rating:
				Intent itent = new Intent(Intent.ACTION_VIEW);
				itent.setData(Uri.parse("market://details?id="
						+ getApplication().getPackageName()));
				startActivity(itent);
				break;
			case R.id.ribbon_menu_newest:
				if (newest != null){
					mAdapter = new MovieAdapter(MoviesActivity.this, newest, movieAdapterListener, cellSize);
					mListView.setAdapter(mAdapter);
					MoviesActivity.search = newest;
				}else{
					showPleaseWait();
					GetMovieNew gm = new GetMovieNew();
					gm.setBase(newest);
					gm.execute(dsMoiRa);
				}
				break;
			case R.id.ribbon_menu_all:
				if (truyen != null){
					showPleaseWait();
					mAdapter = new MovieAdapter(MoviesActivity.this, truyen, movieAdapterListener, cellSize);
					mListView.setAdapter(mAdapter);
					MoviesActivity.search = truyen;
					hidePleaseWait();
				}else{
					showPleaseWait();
					GetMoviesOffline getOffline = new GetMoviesOffline();
					getOffline.execute("getoffline");
				}
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}
	private static long back_pressed;
	@Override
	public void onBackPressed() {
		// If the user is looking at detailed rows, put them back to the other screen instead
		// of leaving the page entirely
		if (mViewPager.getCurrentItem() != 1) {
			mViewPager.setCurrentItem(1, true);
		}
		else {
			if (back_pressed + 2000 > System.currentTimeMillis())
			{
				super.onBackPressed();
			}
			else
			{
				Toast.makeText(this, getString(R.string.doubleClickMsg), Toast.LENGTH_SHORT).show();
				back_pressed = System.currentTimeMillis();
			}
		}
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return new SpaceFragment();
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public long getItemId(int position) {
			return super.getItemId(position);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A simple Fragment that just takes up space; We just want to use
	 * the Decor View on top for display.
	 */
	public static class SpaceFragment extends Fragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_space, container, false);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// MovieAdapterListener

	@Override
	public void onMovieClicked(Movie movie, boolean isOnLeft) {
		int targetItem = isOnLeft ? 0 : 2;
		if (mViewPager.getCurrentItem() != targetItem) {
			mViewPager.setCurrentItem(targetItem, true);
		}
		else
		{
			if (internet.haveNetworkConnection()){
				Intent itent = new Intent(MoviesActivity.this, IndexableListViewActivity.class);
				itent.putExtra("id", movie.getId());
				itent.putExtra("img", movie.getmPosterRes());
				itent.putExtra("name", movie.getTitle());
				startActivity(itent);
			}
			else
			{
				Toast.makeText(MoviesActivity.this, getString(R.string.noInternet), Toast.LENGTH_LONG).show();
			}
		}
	}
	class GetMovies extends AsyncTask<String, Void, JsonNode>{
		private boolean loadImediatly = false;
		public void setLoadImediatly(boolean isSet){
			loadImediatly = isSet;
		}
		@Override
		protected JsonNode doInBackground(String... params) {
    		return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			List<Movie> movies = new ArrayList<Movie>();
			if (result != null){
				Iterator<JsonNode> ite = result.iterator();
				while(ite.hasNext()){
					movies.add(Converter.json2Manga(ite.next()));
				}
			}
			try {
				if (movies.size() > 0){
					MockUtils.writeDataToFileSystem(rootPath + getString(R.string.movieData), movies);					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (loadImediatly){
				mAdapter = new MovieAdapter(MoviesActivity.this, movies, movieAdapterListener, cellSize);
				MoviesActivity.search = movies;
				MoviesActivity.truyen = movies;
				mListView.setAdapter(mAdapter);
			}
			hidePleaseWait();
		}
	}
	
	class GetMovieNew extends AsyncTask<String, Void, JsonNode>{
		private List<Movie> base;
		private boolean isAll = false;
		@Override
		protected JsonNode doInBackground(String... params) {
    		return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			if (result != null){
				Iterator<JsonNode> ite = result.iterator();
				while(ite.hasNext()){
					base.add(Converter.json2Manga(ite.next()));
				}
			}
			mAdapter = new MovieAdapter(MoviesActivity.this, base, movieAdapterListener, cellSize);
			mListView.setAdapter(mAdapter);
			MoviesActivity.search = base;
			if (isAll){
				MoviesActivity.truyen = base;
			}
			hidePleaseWait();
		}

		public List<Movie> getBase() {
			return base;
		}

		public void setBase(List<Movie> base) {
			this.base = base;
		}
	}
	class GetMoviesByType extends AsyncTask<String, Void, JsonNode>{
		private boolean loadImediatly = false;
		private int type;
		public void setType(int typ){
			type = typ;
		}
		public void setLoadImediatly(boolean isSet){
			loadImediatly = isSet;
		}
		@Override
		protected JsonNode doInBackground(String... params) {
			return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			List<Movie> movies = new ArrayList<Movie>();
			if (result != null){
				Iterator<JsonNode> ite = result.iterator();
				while(ite.hasNext()){
					movies.add(Converter.json2Manga(ite.next()));
				}
			}
			/*if (type == 6){
				hanhdong6 = movies;
			}
			if (type == 7){
				vothuat7 = movies;
			}
			if (type == 8){
				phimhai8 = movies;
			}
			if (type == 9){
				phimtinhcam9 = movies;
			}
			if (type == 10){
				kinhdi10 = movies;
			}
			if (type == 11){
				khoahoc11 = movies;
			}
			if (type == 12){
				phieuluu12 = movies;
			}
			if (type == 13){
				phimtamly13 = movies;
			}
			if (type == 13){
				cotrang14 = movies;
			}*/
			if (loadImediatly){
				mAdapter = new MovieAdapter(MoviesActivity.this, movies, movieAdapterListener, cellSize);
				MoviesActivity.search = movies;
				mListView.setAdapter(mAdapter);
			}
			hidePleaseWait();
		}
	}
	/*private void displayFilm(List<Movie> movies){
		try{
			setting = (Setting) MockUtils.readDataFromFileSystem(settingPath);
			if (setting != null && setting.isShuffle()){
				Collections.shuffle(movies);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		mAdapter = new MovieAdapter(MoviesActivity.this, movies, movieAdapterListener, cellSize);
		MoviesActivity.search = movies;
		mListView.setAdapter(mAdapter);
	}*/
	class GetMoviesOther extends AsyncTask<String, Void, JsonNode>{
		private boolean loadImediatly = false;
		private String file ="";
		private String source = "";
		public void setLoadImediatly(boolean isSet){
			loadImediatly = isSet;
		}
		@Override
		protected JsonNode doInBackground(String... params) {
			try{
			file = params[1];
			source = params[2];
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			List<Movie> movies = new ArrayList<Movie>();
			if (result != null){
				Iterator<JsonNode> ite = result.iterator();
				while(ite.hasNext()){
					movies.add(Converter.json2Manga(ite.next()));
				}
			}
			try {
				if (movies.size() > 0){
					MockUtils.writeDataToFileSystem(rootPath + file, movies);					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (loadImediatly){
				mAdapter = new MovieAdapter(MoviesActivity.this, movies, movieAdapterListener, cellSize);
				MoviesActivity.search = movies;
				mListView.setAdapter(mAdapter);
			}
			/*if (source != null && !source.isEmpty()){
				if (source.equals("phimbo")){
					MoviesActivity.phimbo = movies;
				}else if (source.equals("phimle")){
					MoviesActivity.phimle = movies;
				}else if (source.equals("clip")){
					MoviesActivity.clip = movies;
				}else if (source.equals("hoathinh")){
					MoviesActivity.hoathinh = movies;
				}
			}*/
			hidePleaseWait();
		}
	}
	class GetMoviesOffline extends AsyncTask<String, Void, List<Movie>>{
		@Override
		protected List<Movie> doInBackground(String... params) {
			List<Movie> m = new ArrayList<Movie>();
			try {
				
				m = (ArrayList<Movie>)MockUtils.readDataFromFileSystem(rootPath + getString(R.string.movieData));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception e){
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			}catch(Error e){
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			}
    		return m;
		}
		
		@Override
		protected void onPostExecute(List<Movie> result) {
			if (!result.isEmpty())
			{
				MoviesActivity.truyen = result;
				GetMovieNew gm = new GetMovieNew();
				gm.setBase(result);
				gm.isAll = true;
				gm.execute(truyenMoi);
				doUpdateApp();
				return;
			}else if (internet.haveNetworkConnection()){
				GetMovies gMovie = new GetMovies();
				gMovie.setLoadImediatly(true);
				gMovie.execute(truyenTranhLink);
			}else{
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_SHORT).show();
				hidePleaseWait();
			}			
		}
	}
	class GetManga extends AsyncTask<String, Void, JsonNode>{
		private boolean loadImediatly = false;
		public void setLoadImediatly(boolean isSet){
			loadImediatly = isSet;
		}
		@Override
		protected JsonNode doInBackground(String... params) {
    		return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			List<Movie> movies = new ArrayList<Movie>();
			if (result != null){
				Iterator<JsonNode> ite = result.iterator();
				while(ite.hasNext()){
					movies.add(Converter.json2Manga(ite.next()));
				}
			}
			try {
				if (movies.size() > 0){
					MockUtils.writeDataToFileSystem(rootPath + getString(R.string.movieSeries), movies);					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (loadImediatly){
				mAdapter = new MovieAdapter(MoviesActivity.this, movies, movieAdapterListener, cellSize);
				MoviesActivity.search = movies;
				mListView.setAdapter(mAdapter);
			}
			hidePleaseWait();
		}
	}
	class GetMoviesOfflineOther extends AsyncTask<String, Void, List<Movie>>{
		private String source = "";
		@Override
		protected List<Movie> doInBackground(String... params) {
			List<Movie> m = new ArrayList<Movie>();
			try {
				m = (ArrayList<Movie>)MockUtils.readDataFromFileSystem(rootPath + params[0]);
				source = params[1];
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception e){
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			}catch(Error e){
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			}
			return m;
		}
		
		@Override
		protected void onPostExecute(List<Movie> result) {
			if (!result.isEmpty())
			{
				mAdapter = new MovieAdapter(MoviesActivity.this, result, movieAdapterListener, cellSize);
				mListView.setAdapter(mAdapter);
				MoviesActivity.search = result;
				hidePleaseWait();
				return;
			}else if (internet.haveNetworkConnection()){
				GetMovies gMovie = new GetMovies();
				gMovie.setLoadImediatly(true);
				gMovie.execute(truyenTranhLink);
			}else{
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_SHORT).show();
				hidePleaseWait();
			}			
		}
	}
	
	@Override
	public void RibbonMenuItemClick(int itemId) {
		loadAdview();
		switch (itemId) {
		case R.id.ribbon_menu_newestMovies:
			if (newest != null && newest.size() > 0){
				mAdapter = new MovieAdapter(MoviesActivity.this, newest, movieAdapterListener, cellSize);
				mListView.setAdapter(mAdapter);
				MoviesActivity.search = newest;
			}else{
				showPleaseWait();
				GetMovieNew gm = new GetMovieNew();
				gm.setBase(newest);
				gm.execute(dsMoiRa);
			}
			break;
		case R.id.ribbon_menu_allMovies:
			if (truyen != null){
				showPleaseWait();
				mAdapter = new MovieAdapter(MoviesActivity.this, truyen, movieAdapterListener, cellSize);
				mListView.setAdapter(mAdapter);
				MoviesActivity.search = truyen;
				hidePleaseWait();
			}else{
				showPleaseWait();
				GetMoviesOffline getOffline = new GetMoviesOffline();
				getOffline.execute("getoffline");
			}
			break;
		case R.id.ribbon_menu_settings:
			Intent ta = new Intent(MoviesActivity.this, SettingsActivity.class);
			startActivity(ta);
			break;
		case R.id.ribbon_menu_rating:
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=" + getApplication().getPackageName()));
			startActivity(intent);
			break;
		case R.id.ribbon_menu_about:
			DialogFragment df = new AboutDialogFragment();
			df.show(getSupportFragmentManager(), "aboutDf");
		}
	}
}
