package com.dreamsoft.manga.series.util;

import com.dreamsoft.manga.series.data.Movie;
import com.dreamsoft.manga.series.data.UpdateApp;
import com.fasterxml.jackson.databind.JsonNode;

public class Converter {
	public static UpdateApp json2UpdateApp(JsonNode node){
		UpdateApp updateApp = null;
		try
        {
            if (node != null){
            	updateApp = new UpdateApp();
            	if (node.get("type") != null){
            		updateApp.setType(node.get("type").asText());
            	}
            	if (node.get("action") != null){
            		updateApp.setAction(node.get("action").asText());
            	}
            	if (node.get("messenger") != null){
            		updateApp.setMessage(node.get("messenger").asText());
            	}
            }
        }
        catch (Exception e)
		{
			e.printStackTrace();
		}
		return updateApp;
	}
	public static Movie json2MovieOld(JsonNode m){
		Movie movie = new Movie();
		if (m.get("name") != null)
		{
			movie.setTitle(m.get("name").asText());
		}
		if (m.get("poster") != null)
		{
			movie.setmPosterRes(m.get("poster").asText());
		}
		if (m.get("rating") != null)
		{
			movie.setFilmRating(m.get("rating").asText());
		}
		if (m.get("score") != null)
		{
			movie.setScore(m.get("score").asDouble());
		}
		if (m.get("showtimes") != null)
		{
			movie.setShowTimes(m.get("showtimes").asText());
		}else{
			movie.setShowTimes("Manga");
		}
		if (m.get("title") != null)
		{
			movie.setTitle(m.get("title").asText());
		}
		if (m.get("id") != null)
		{
			movie.setId(m.get("id").asInt());
		}
		return movie;
	}
	public static Movie json2Manga(JsonNode m){
		Movie movie = new Movie();
		if (m.get("name") != null)
		{
			movie.setTitle(m.get("name").asText());
		}
		if (m.get("image") != null)
		{
			movie.setmPosterRes(m.get("image").asText());
		}
		if (m.get("img") != null)
		{
			movie.setmPosterRes(m.get("img").asText());
		}
		if (m.get("id") != null)
		{
			movie.setId(m.get("id").asInt());
		}
		movie.setScore(5.0);
		movie.setShowTimes("Manga");
		movie.setFilmRating("HD");
		if (m.get("chapter") != null){
			movie.setShowTimes(m.get("chapter").asText());
		}
		return movie;
	}
}
